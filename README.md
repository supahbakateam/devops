# README #

Ce readme montrera comment déployer le projet en utilisant Vagrant / Docker / Jenkins / Ansible.

### Equipe ###

* VOIROL Adrien
* MONTES Cyril
* SAN JOSE Quentin

### Prérequis ###

* Vagrant (Conseillé : 1.9.1)
* VirtualBox (Conseillé 5.1.14)

### But du projet ###

Le but du projet est d'avoir une interface permettant le paiement par CB d'un montant que l'on souhaite pour une entreprise. Cela peut être un don ou un équivalent. Au bout d'une semaine, le montant est prélevé. On vérifie si l'utilisateur a assez d'argent avant de pouvoir accepter son paiement. La carte est aussi vérifiée.
L'application devra être développée et intégrée de manière continue. Elle aura des tests unitaires et d'acceptation. L'application sera buildée grâce à Jenkins.

### Comment utiliser le projet ? ###

* Extraire le dossier `devops`
* Lancer un `vagrant up`, cela peut prendre plusieurs minutes
* Accéder à Jenkins via cette [url](http://localhost:8080/)
* Accéder à la configuration du slave [CD](http://localhost:8080/computer/CD/configure)
+ Lui ajouter ses credentials
    * _Kind_ : `Username with password`
    * _Scope_ : `Global` 
    * _Utilisateur_ : `vagrant`
    * _Mot de passe_ : `vagrant`
* Sauvegarder
* Launch slave agent
* Ensuite, aller sur le [projet](http://localhost:8080/job/devops-service/) et lancer un build