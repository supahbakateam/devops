<?php 

$I = new AcceptanceTester($scenario);
$I->am('user');
$I->wantTo('pay 30$ with my valid credid card');
$I->lookForwardTo('my payment is accepted');
$I->amOnPage('form.html');

$I->fillField('cardNumber', "2156435208576435");
$I->fillField('mois', "05");
$I->fillField('annee', "17");
$I->fillField('cvv', "256");
$I->fillField('ammount', "30");
$I->click("Valider");


$I->see("30€ envoyés.");
