<?php 

$I = new AcceptanceTester($scenario);
$I->am('user');
$I->wantTo('pay 5000$ with my valid credid card');
$I->lookForwardTo('my payment isn\'t accepted');
$I->amOnPage('form.html');

$I->fillField('cardNumber', "2156435208576435");
$I->fillField('mois', "05");
$I->fillField('annee', "17");
$I->fillField('cvv', "256");
$I->fillField('ammount', "5000");
$I->click("Valider");


$I->see("Vous essayez de dépenser trop d'argent.");
