<?php


class FakeDbReader {
   function __construct($datas) {
      $this->datas = $datas;
      $this->index = 0;
   }

   function fetch() {
      if(count($this->datas) == $this->index)  return null;
      return $this->datas[$this->index++];
   }
}

class TransationApplicationTest extends PHPUnit_Framework_TestCase
{
   private $card;

   function __construct(){
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $this->card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);
   }
   public function testcompareBDD() {
      $response = [];
      $response["id"] = 1;
      $response["expirationDate"] = "2017-03-03";
      $response["cardNumber"] = "2156435208576435";
      $response["CVV"] = "214";
      $response["ammount"] = 300;
      
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserWithCardNumber"])->disableOriginalConstructor()->getMock();
      $db->method("getUserWithCardNumber")->will($this->returnValue($response));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->never())->method("shutdown");

      $this->assertEquals(1, $app->compareBDD($this->card, $db));
   }

   public function testcompareBDDCardNumberNotFound() {
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserWithCardNumber"])->disableOriginalConstructor()->getMock();
      $db->method("getUserWithCardNumber")->will($this->returnValue(null));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->once())->method("shutdown");

      $app->compareBDD($this->card, $db);
   }

   public function testcompareBDDCvvError() {
      $response = [];
      $response["id"] = 1;
      $response["expirationDate"] = "2017-03-03";
      $response["cardNumber"] = "2156435208576435";
      $response["CVV"] = "254";
      $response["ammount"] = 300;
      
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserWithCardNumber"])->disableOriginalConstructor()->getMock();
      $db->method("getUserWithCardNumber")->will($this->returnValue($response));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->once())->method("shutdown");

      $app->compareBDD($this->card, $db);
   }

   public function testcompareBDDDateError() {
      $response = [];
      $response["id"] = 1;
      $response["expirationDate"] = "2017-04-04";
      $response["cardNumber"] = "2156435208576435";
      $response["CVV"] = "254";
      $response["ammount"] = 300;
      
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserWithCardNumber"])->disableOriginalConstructor()->getMock();
      $db->method("getUserWithCardNumber")->will($this->returnValue($response));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->once())->method("shutdown");

      $app->compareBDD($this->card, $db);
   }

   public function testCompareAmmountValid(){
      $user = [];
      $user["id"] = 1;
      $user["expirationDate"] = "2017-03-03";
      $user["cardNumber"] = "2156435208576435";
      $user["CVV"] = "214";
      $user["ammount"] = 300;
      
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserById", "getWaitingTransaction"])->disableOriginalConstructor()->getMock();
      $db->method("getUserById")->will($this->returnValue($user));
      $db->method("getWaitingTransaction")->will($this->returnValue([]));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->never())->method("shutdown");

      $this->assertEquals(true, $app->compareAmmount(60, 1, $db));
   }

   public function testCompareAmmountNoAvailableTotal(){
      $user = [];
      $user["id"] = 1;
      $user["expirationDate"] = "2017-03-03";
      $user["cardNumber"] = "2156435208576435";
      $user["CVV"] = "214";
      $user["ammount"] = 250;
      
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["getUserById", "getWaitingTransaction"])->disableOriginalConstructor()->getMock();
      $db->method("getUserById")->will($this->returnValue($user));
      $db->method("getWaitingTransaction")->will($this->returnValue([]));

      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $app->expects($this->once())->method("shutdown");

      $app->compareAmmount(60, 1, $db);
   }

	public function testSendMoney()
	{
      $db = $this->getMockBuilder(\DevOps\Database::class)->setMethods(["insertTransaction"])->disableOriginalConstructor()->getMock();
      $db->method("insertTransaction")->will($this->returnValue(1));
      
      $app = $this->getMockBuilder(DevOps\TransationApplication::class)->setMethods(["shutdown"])->getMock();
      $db->expects($this->once())->method("insertTransaction");
      $this->assertEquals(1, $app->sendMoney(60, 1, $db));
	}
}