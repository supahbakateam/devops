<?php

class CardTest extends PHPUnit_Framework_TestCase
{
   public function testCardInit(){

      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals($cardNumber, $card->getCardNumber());
      $this->assertEquals($dateExpiration, $card->getDateExpiration());
      $this->assertEquals($cvv, $card->getCvv());
   }

   //test Card Number
   public function testValidCardNumber() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(true, $card->isValidCardNumber());
   }

   public function testCardNumberNotNumber() {
      $cardNumber = 'aaaabbbbccccdddd';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCardNumber());
   }

   public function testCardNumberLengthError() {
      $cardNumber = '111';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCardNumber());
   }

   public function testCardNumberNull() {
      $cardNumber = null;
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCardNumber());
   }

   //test cvv
   public function testValidCvv() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(true, $card->isValidCvv());
   }

   public function testCvvNotNumber() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = 'aaa';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCvv());
   }

   public function testCvvLengthError() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '2145';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCvv());
   }

   public function testCvvNull() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = null;
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCvv());
   }

   
   //test date
   public function testValidDate() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(true, $card->isValidCvv());
   }

   public function testDateExpired() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/16");
      $cvv = 'aaa';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCvv());
   }

   public function testDateNull() {
      $cardNumber = '2554785419301547';
      $dateExpiration = null;
      $cvv = null;
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValidCvv());
   }

   //testGlobalValidation
   public function testValidGlobal() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(true, $card->isValid());
   }

   public function testInvalidGlobalWithCardNumberError() {
      $cardNumber = '2554785411547';
      $dateExpiration = date("03/17");
      $cvv = '214';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValid());
   }

   public function testInvalidGlobalWithCvvError() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/17");
      $cvv = '2414';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValid());
   }
   public function testInvalidGlobalWithDateExprire() {
      $cardNumber = '2554785419301547';
      $dateExpiration = date("03/16");
      $cvv = '244';
      $card = new DevOps\Card($cardNumber, $dateExpiration, $cvv);

      $this->assertEquals(false, $card->isValid());
   }
}