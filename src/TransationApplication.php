<?php

namespace DevOps;

class TransationApplication
{
	public function isNull($value, $error){
		if($value!=null)
		{
			return true;
		}
		else
		{
			$this->shutdown("$".$error." est null.");
		}
	}
	public function shutdown($errorMessage){
		echo $errorMessage;
		exit();
	}
	public function compareBDD($card, $db)
	{
		$res = $db->getUserWithCardNumber($card->getCardNumber());
      if($res == null){
			return $this->shutdown("Le numero de carte n'est pas correct.");
      }

      $idBDD = $res['id'];
      $expirationDateBDD = $res['expirationDate'];
      $expirationDateBDD = new \DateTime($expirationDateBDD);
      $expirationDateBDD = $expirationDateBDD->format('m/y');
      $cvvBDD = $res['CVV'];
		if($expirationDateBDD != $card->getDateExpiration())
		{
			$this->shutdown("La date d'expiration n'est pas correcte.");
		}
		else if($cvvBDD != $card->getCVV())
		{
			$this->shutdown("Le cvv n'est pas correct.");
		}
		else
		{
			return $idBDD;
		}
	}

   public function getDepenseAccount($db, $id){
      $waitings = $db->getWaitingTransaction($id);
      $depense = 0;
      foreach($waitings as $waiting){
         $depense += $waiting["ammount"];
      }
      return $depense;
   }

	public function compareAmmount($ammount, $id, $db)
	{
      $res = $db->getUserById($id);
      $total = $res['ammount'];
      $depense = $this->getDepenseAccount($db, $id);
		$multipl = 5;
		if($ammount*$multipl > $total - $depense)
		{
			$this->shutdown("Vous essayez de dépenser trop d'argent.");
		}
		else
		{
			return true;
		}
	}

	public function sendMoney($ammount, $id, $db)
	{
		return $db->insertTransaction($id, $ammount);
	}
}