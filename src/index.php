<?php
namespace DevOps;
require "../vendor/autoload.php";

if(isset($_POST["cardNumber"]) && $_POST["cardNumber"] != null)
{
	$cardNumber = $_POST["cardNumber"];
}
else
{
	$cardNumber = '2156435208576435';
}
if(isset($_POST["mois"]) && $_POST["mois"] != null)
{
	$moisExpiration = $_POST["mois"];
}
else
{
	$moisExpiration = "05";
}
if(isset($_POST["annee"]) && $_POST["annee"] != null)
{
	$anneeExpiration = $_POST["annee"];
}
else
{
	$anneeExpiration = "17";
}
$dateExpiration = $moisExpiration."/".$anneeExpiration;
if(isset($_POST["cvv"]) && $_POST["cvv"] != null)
{
	$cvv = $_POST["cvv"];
}
else
{
	$cvv = "256";
}
if(isset($_POST["ammount"]) && $_POST["ammount"] != null)
{
	$ammount = $_POST["ammount"];
}
else
{
	$ammount = 30;
}
$devops = new TransationApplication;
$devops->isNull($cardNumber, "cardNumber");
$devops->isNull($moisExpiration, "moisExpiration");
$devops->isNull($anneeExpiration, "anneeExpiration");
$devops->isNull($cvv, "cvv");
$devops->isNull($ammount, "ammount");
if(!$ammount > 0)
{
	$devops->shutdown("Montant entré incorrect.");
}
$db = new Database();

$card = new Card($cardNumber, $dateExpiration, $cvv);
if(!$card->isValid())
{
	$devops->shutdown($card->getFirstError());
}
$id = $devops->compareBDD($card, $db);
$devops->compareAmmount($ammount, $id, $db);
//sendMoney($ammount, $id, $db);
echo $ammount."€ envoyés.";
?>