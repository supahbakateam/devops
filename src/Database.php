<?php

namespace DevOps;

class Database {
   public $db;

   public static function connectBDD(){
      try
      {
         return new \PDO('mysql:host=localhost; dbname=devopsmoney', 'root', 'toor', array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
      }
      catch(Exception $e)
      {
         return null;
      }
   }

   function __construct(){
      $this->db = self::connectBDD();
   }

   function getUserWithCardNumber($cardNmber){
      $sql="SELECT * FROM users WHERE cardNumber = ".$cardNmber . " LIMIT 1;";
		$req=$this->db->query($sql);
      return $req->fetch();
   }

   function getUserById($id) {
		$sql="SELECT * FROM users WHERE id = ".$id;
		$req=$this->db->query($sql);
		while ($res=$req->fetch())
		{
			return $res;//$total = $res['ammount'];
		}
   }
   
   function getWaitingTransaction($idUser){
		$sql = "SELECT * FROM enattente WHERE idUser = " . $idUser;
		$req = $this->db->query($sql);
      return $req->fetchAll();
   }

   function insertTransaction($id, $ammount){
		$sql = "INSERT INTO enattente(idUser, ammount, date) VALUES($id, $ammount, now())";
		return $this->db->exec($sql);
   }
}