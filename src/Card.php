<?php
namespace DevOps;

class Card
{
   private $errors = [];
   public function getErrors()
   {
      return $this->errors;
   }
   public function getFirstError()
   {
   return $this->errors[0];
   }

   private $cardNumber;
   public function getCardNumber()
   {
      return $this->cardNumber;
   }
   public function setCardNumber($value)
   {
      $this->cardNumber = $value;
   }

   private $dateExpiration;
   public function getDateExpiration()
   {
      return $this->dateExpiration;
   }
   public function setDateExpiration($value)
   {
      $this->dateExpiration = $value;
   }

   private $cvv;
   public function getCvv()
   {
      return $this->cvv;
   }
   public function setCvv($value)
   {
      $this->cvv = $value;
   }

   public function __construct($card, $date, $cv)
   {
      $this->setCardNumber($card);
      $this->setDateExpiration($date);
      $this->setCvv($cv);
   }

   public function isValid()
   {
      return $this->isValidCardNumber() && $this->isValidCvv() && $this->isValidDateExpiration();
   }

   public function isValidCardNumber(){
      $cardNumber = $this->getCardNumber();
      return $this->isANumber($cardNumber, "Le numero de carte") && $this->hasNumber($cardNumber, 16, "Le numero de carte");
   }

   public function isValidCvv(){
      $cvv = $this->getCvv();
      return $this->isANumber($cvv, "Le cvv") && $this->hasNumber($cvv, 3, "Le cvv");
   }

   public function isValidDateExpiration(){
      return $this->isDateValid($this->getDateExpiration()) && $this->isExpired($this->getDateExpiration());
   }


   function isANumber($value, $errorMessage)
   {
      if (is_numeric($value))
      {
         return true;
      }
      else
      {
         $this->errors[] = $errorMessage . " n'est pas un int.";
         return false;
      }

   }
   function hasNumber($value, $limitNumber, $errorMessage)
   {
      if(strlen($value) == $limitNumber)
      {
         return true;
      }
      else
      {
         $this->errors[] = $errorMessage." n'a pas le nombre de chiffre requis.";
         return false;
      }
   }
   function isDateValid($value)
   {
      $value = explode("/", $value); 
	  return $this->isANumber($value[0], "Le mois") && $this->isANumber($value[1], "L'annee") && $this->hasNumber($value[0], 2, "Le mois") && $this->hasNumber($value[1],2, "L'annee");
   }
   function isExpired($value)
   {
      $now = date('m/y');
      $value = explode("/", $value); 
      $now = explode("/", $now); 
      $value = $value[1].$value[0];
      $now = $now[1].$now[0];
      if($now <= $value)
      {
         return true;
      }
      else
      {
         $this->errors[] = "La carte est périmée.";
         return false;
      }
   }
}
?>