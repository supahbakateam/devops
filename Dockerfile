FROM tutum/lamp:latest
RUN rm -fr /app && git clone https://bitbucket.org/supahbakateam/devops.git /app
RUN cp /app/devops.conf /etc/apache2/sites-available/devops.conf
RUN a2ensite devops.conf
RUN service apache2 restart
EXPOSE 80 80
CMD ["/run.sh"]