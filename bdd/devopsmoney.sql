-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 09 Mars 2017 à 12:32
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `devopsmoney`
--

-- --------------------------------------------------------

--
-- Structure de la table `enattente`
--

CREATE TABLE `enattente` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `ammount` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `cardNumber` text NOT NULL,
  `expirationDate` date NOT NULL,
  `CVV` text NOT NULL,
  `ammount` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `cardNumber`, `expirationDate`, `CVV`, `ammount`) VALUES
(1, '2156435208576435', '2017-05-09', '256', 300),
(2, '5427013269587542', '2017-05-09', '589', 300),
(3, '3254865214963256', '2017-06-16', '745', 300),
(4, '5423687524556213', '2017-03-31', '981', 300),
(5, '3245109677458924', '2017-06-22', '209', 300),
(6, '3658954751245896', '2017-09-15', '732', 300),
(7, '5421325279276471', '2017-08-12', '659', 300),
(8, '8732659458213265', '2017-08-05', '442', 300),
(9, '8732659458213265', '2017-08-05', '442', 300),
(10, '6653295874129554', '2017-07-31', '117', 300);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `enattente`
--
ALTER TABLE `enattente`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `enattente`
--
ALTER TABLE `enattente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
